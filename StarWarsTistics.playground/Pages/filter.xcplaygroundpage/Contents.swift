//: # STARWARStistics

import Foundation
import StarWars

/*:
## Let’s generate some STARWARStistics!

Star Wars data courtesy of Star Wars API (SWAPI), stored offline to circumvent epic #demofail

### Step 1: `filter`

Similar to ObjC’s `[NSArray filteredArrayWithPredicate:]`

![filter](filter.png)
*/


//: Q1: Which characters have appeared in 6 films?

print(people.filter { $0.films.count == 6 })


//: [Previous](@previous) | [Next](@next)
