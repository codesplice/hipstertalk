//: # STARWARStistics

import Foundation
import StarWars

/*:
### Step 2: `map`

Convert an array of items into an array of different items by applying a transform function.

If this was in ObjC, it would be called `[NSArray arrayByTransformingObjectsUsingBlock:]`

![map](map.png)

*/

//: Q2: What species appear in the first film?

print(films[0].species.map(Species.init))

//: Q3: Who were the characters in 4 or more films, what films were they in?


print(people.filter { $0.films.count >= 4 }.map {p in
  return (p.name, p.films.map(Film.init))
})


//: [Previous](@previous) | [Next](@next)
