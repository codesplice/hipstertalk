//: # STARWARStistics

import Foundation
import StarWars

/*:
### Step 3: `reduce`

Perform an aggregation operation on all the items in the array

![reduce](reduce.png)

*/

//: Q4: What is the total length of all starships laid end-to-end?

let totalLength = starships.map { $0.length }.reduce(0, combine: +)
print(totalLength)


//: Q5: Group all planets by their climate (i.e. produce a dictionary [String: [Planet]])


let planetsByClimate = planets.groupBy { $0.climate }
print(planetsByClimate)


//: [Previous](@previous)

