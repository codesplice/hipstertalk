/*:
# Collection Pipelines

![pipeline](pipeline.jpg)

This refers to using *higher order functions* instead of loops to query & manipulate collections.

* small, reusable operations chained together
* available in most modern languages (not ObjC)
* Swift has built-in support


[Next](@next)
*/
