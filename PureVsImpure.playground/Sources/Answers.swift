import UIKit

public enum MathError: ErrorType {
    case DivideByZero
}

public enum Example {
    case One, Two, Three, Four, Five, Six
}

public struct PureImpureAnswer: CustomPlaygroundQuickLookable {
    let explanation: String
    let image: UIImage
    
    public func customPlaygroundQuickLook() -> PlaygroundQuickLook {
        UIGraphicsBeginImageContext(CGSize(width: 500, height: 600))
        image.drawInRect(CGRectMake(0, 0, 500, 500))
        let style = NSMutableParagraphStyle()
        style.alignment = .Center
        NSString(string: explanation).drawInRect(CGRectMake(10, 500, 480, 100), withAttributes:[NSFontAttributeName: UIFont.boldSystemFontOfSize(24), NSParagraphStyleAttributeName: style])
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return PlaygroundQuickLook.Image(img)
    }
}

let pure = UIImage(named: "pure.jpg")!
let impure = UIImage(named: "impure.jpg")!
let borderline = UIImage(named: "borderline.jpg")!
let answers = [
    Example.One: PureImpureAnswer(explanation: "Accessing the current time, location, random numbers, network, file system, database etc is IMPURE", image: impure),
    .Two: PureImpureAnswer(explanation:"Passing the non-deterministic elements as parameters is a common pattern", image: pure),
    .Three: PureImpureAnswer(explanation:"We’re changing mutable state within the body of this function, AND we’ll get a different id each time", image: impure),
    .Four: PureImpureAnswer(explanation:"BORDERLINE - technically writing to stdout is a side-effect, but I try not to be too religious about it", image: borderline),
    .Five: PureImpureAnswer(explanation:"We’ll get a runtime error if passing 0, however this is still a PURE function - just a PARTIAL function", image: pure),
    .Six: PureImpureAnswer(explanation:"We always get the same output for the same pameters, and there are no side-effects ", image: pure),
]

public func showAnswer(e: Example) -> PureImpureAnswer {
    return answers[e]!
}

public func answerNumberOne() -> PureImpureAnswer {
    return PureImpureAnswer(explanation:"Accessing the current time, random numbers, network, file system, database etc is IMPURE",
        image: UIImage(named: "impure.jpg")!)
}

public func answerNumberTwo() -> PureImpureAnswer {
    return PureImpureAnswer(explanation:"Passing the non-deterministic elements as parameters is a common pattern",
        image: UIImage(named: "pure.jpg")!)
}