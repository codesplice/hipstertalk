/*:
## Pure or Impure?
### Example 3
*/

import Foundation

var currentIndex = 0

func getNextIndex() -> Int {
    currentIndex++
    return currentIndex
}



showAnswer(.Three)



//: [Previous](@previous) | [Next](@next)
