/*:
## Pure or Impure?
### Example 4
*/

import Foundation

func performAwesomeCalculation(left: Int, right: Int) -> Int {
    print("Just about to run my awesome calc with parameters \(left) and \(right)")
    return left * right - 1
}


showAnswer(.Four)



//: [Previous](@previous) | [Next](@next)
