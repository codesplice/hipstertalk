/*: 

# Pure vs Impure Functions

![water](water.jpg)

### A Pure Function
1. has no side-effects
2. always returns the same output for the same parameters

Pure functions are important!



[Next](@next)
*/
