/*:
## Let’s play a game: “Pure or Impure”
(not “Global Thermonuclear War”!)
*/

import Foundation

/*:
### Example 1
*/

func fiveMinutesFromNow() -> NSDate {
    return NSDate().dateByAddingTimeInterval(60 * 5)
}


showAnswer(.One)


//: [Previous](@previous) | [Next](@next)
