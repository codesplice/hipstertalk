/*: 
## Pure or Impure?
### Example 2
*/

import Foundation

func fiveMinutesFromDate(date: NSDate) -> NSDate {
    return date.dateByAddingTimeInterval(60 * 5)
}



showAnswer(.Two)


//: [Previous](@previous) | [Next](@next)
