/*:
## Pure or Impure?
### Example 5
*/

import Foundation

func divide(left: Int, _ right: Int) -> Int {
    return left / right
}



showAnswer(.Five)



//: [Previous](@previous) | [Next](@next)
