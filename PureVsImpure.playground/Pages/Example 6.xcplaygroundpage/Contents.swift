/*:
## Pure or Impure?
### Example 6
*/

import Foundation

func safeDivide(left: Int, _ right: Int) throws -> Int {
    guard right != 0 else { throw MathError.DivideByZero }
    return left / right
}



showAnswer(.Six)



//: [Previous](@previous)
