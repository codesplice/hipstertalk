# HipsterTalk

This is the playground used in my [‘Hipster Functional Programming in Swift’](https://www.youtube.com/watch?v=WlcEBoQ0BNA) talk at DevWorld 2015, for those that want to play along at home.

It has a carthage dependency, so you’ll need to `carthage bootstrap --no-use-binaries` to check out the dependencies, then make sure you build the 'StarWars-OSX' scheme prior to running that playground.