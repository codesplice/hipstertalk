import Foundation

let dateFomatter: NSDateFormatter = {
    let f = NSDateFormatter()
    f.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm"
    f.timeZone = NSTimeZone(forSecondsFromGMT: 60 * 60 * 10)
    return f
}()

func formatTime(time: NSTimeInterval) -> String {
    let hours = Int(time) / (60 * 60)
    let minutes = (Int(time) - hours * 60 * 60) / 60
    return String(format: "%02d:%02d", hours, minutes)
}

let beerTime = dateFomatter.dateFromString("2015-09-01 17:35")!

public func timeCheck() -> String {
    let time = formatTime(beerTime.timeIntervalSinceNow)
    return "🍸⌚️ \(time)"
}