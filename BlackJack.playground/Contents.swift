//: # Let’s play a game!
//: ### (still not Global Thermonuclear War)

import Foundation

/*:
## BlackJack

’Coz gambling apps make more money. I assume everyone knows the rules.

### Types

* Value types are your friend. `struct`s, `enum`s and tuples will help you write more functional code.
* Try to define types so invalid data can’t be represented.
* Functional code tends to separate types from logic. Swift extensions are great for this.
*/

typealias Deck = [PlayingCard]
typealias Hand = [PlayingCard]

struct PlayingCard {
    let suit: Suit
    let rank: Rank
}

enum Suit: String {
    case Hearts, Clubs, Spades, Diamonds
    static let allSuits = [Suit.Hearts, .Clubs, .Spades, .Diamonds]
}

enum Rank: Int {
    case Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten
    case Jack, Queen, King, Ace
    static let allRanks = [Rank.Two, .Three, .Four, .Five, .Six,
        .Seven, .Eight, .Nine, .Ten, .Jack, .Queen, .King, .Ace]
}

enum PlayerType {
    case Player(name: String, hand: Hand)
    case Dealer(Hand)
}

struct Game {
    let deck: Deck
    let turn: Int
    let players: [PlayerType]
}

//: `CustomDebugStringConvertible`/`CustomStringConvertible` support, to help keep us sane.
//: Seriously, define this on everything, you’ll thank me later.

extension PlayingCard: CustomStringConvertible {
    var description: String {
        return "\(rank) of \(suit)"
    }
}

extension Rank: CustomStringConvertible {
    var description: String {
        switch self {
        case Jack:
            return "Jack"
        case Queen:
            return "Queen"
        case King:
            return "King"
        case Ace:
            return "Ace"
        default:
            return self.rawValue.description
        }
    }
}

extension PlayerType: CustomDebugStringConvertible {
    var debugDescription: String {
        return "*\(name)*\n\(hand)\n\n"
    }

    var name: String {
        switch self {
        case let .Player(n, _):
            return n
        case .Dealer:
            return "Dealer"
        }
    }
    
    var hand: Hand {
        switch self {
        case let .Player(_, h):
            return h
        case let .Dealer(h):
            return h
        }
    }
}

extension Game: CustomDebugStringConvertible {
    var debugDescription: String {
        let playerHands = players.map { $0.debugDescription }.joinWithSeparator("\n")
        return "*Deck*\n\(deck)\n\n\(playerHands)"
    }
        
    var dealer: PlayerType {
        return players.filter {
            if case .Dealer = $0 {
                return true
            } else {
                return false
            }
        }.first!
    }
}


//: ### First: we need to generate a deck of cards

// Use this function to help DEAL with the problem
func cartesianProduct<A: SequenceType, B: SequenceType>(fst: A, _ snd: B) -> [(A.Generator.Element, B.Generator.Element)] {
    return fst.flatMap { f in snd.map { s in (f, s) }}
}

func newDeck() -> Deck {
    return cartesianProduct(Suit.allSuits, Rank.allRanks).map(PlayingCard.init)
}

//: ### Now let’s implement some game actions.
//: We need to deal two cards each to player & dealer, and give the ability to hit & stand

// This is going to come in HANDy. The built-in functions just don’t CUT it.
extension Array  {
    func split(n: Int) -> ([Element], [Element]) {
        guard n <= self.count else { fatalError("Run out of elements!") }
        return (Array(self.prefix(n)), Array(self.suffixFrom(n)))
    }
}

extension PlayerType {
    func addCard(card: PlayingCard) -> PlayerType {
        switch self {
        case let .Player(name, hand):
            return .Player(name: name, hand: hand + [card])
        case let .Dealer(hand):
            return .Dealer(hand + [card])
        }
    }
}

extension Game {
    static func start(deck: Deck, players: [String]) -> Game {
        return Game(deck: deck, turn: 0, players: players.map{ .Player(name: $0, hand: []) } + [.Dealer([])])
    }
    
    func deal() -> Game {
        let (cards, remainingDeck) = deck.split(players.count)
        let newHands = zip(players, cards).map { (p, c) in p.addCard(c) }
        return Game(deck: remainingDeck, turn: 0, players: newHands)
    }
    
    func hit() -> Game {
        let (cards, remainingDeck) = deck.split(1)
        var tempPlayers = players
        tempPlayers[turn] = tempPlayers[turn].addCard(cards[0])
        return Game(deck: remainingDeck, turn: turn, players: tempPlayers)
    }
    
    func stand() -> Game {
        if turn < players.count - 2 {
            return Game(deck: deck, turn: turn + 1, players: players)
        } else {
            var game = Game(deck: deck, turn: turn + 1, players: players)
            while game.dealer.score != nil && game.dealer.score <= 16 {
                game = game.hit()
            }
            return game

        }
    }
}



//: ### Now we need to implement scoring! This should be a piece of cake
//: (Famous last words)

extension PlayingCard {
    var score: [Int] {
        switch rank {
        case .Jack, .Queen, .King:
            return [10]
        case .Ace:
            return [1,11]
        default:
            return [rank.rawValue]
        }
    }
}

extension PlayerType {
    var score: Int? {
        let possibleScores = hand.map { $0.score }.reduce([0]) { l, r in
            cartesianProduct(l, r).map { (l, r) in l + r }
        }
        return possibleScores.filter { $0 <= 21 }.sort(>).first
    }
    
    var scoreDescription: String {
        return score?.description ?? "Bust!"
    }
}

typealias GameOutcome = (win:[String], lose:[String])

extension Game {
    var outcome: GameOutcome {
        let otherPlayers = players.filter { $0.name != "Dealer" }
        let winners = otherPlayers.filter { $0.score != nil && $0.score >= dealer.score ?? 0 }
            .map { $0.name }
        let losers = players.filter { $0.score == nil || $0.score < dealer.score ?? 0 }
            .map { $0.name }
        return (win: winners, lose: losers)
    }
}

//IMPURE
let deck = newDeck().shuffle()
var game = Game.start(deck, players: ["Sam", "Tim"])
game = game.deal().deal()
game = game.hit().stand()
game = game.stand()
print(game)
for p in game.players {
    print("\(p.name): \(p.scoreDescription)")
}

print("Winners: \(game.outcome.win)")
print("Losers: \(game.outcome.lose)")

