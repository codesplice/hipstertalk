//
//  StarWars-OSX.h
//  StarWars-OSX
//
//  Created by Sam Ritchie on 29/08/2015.
//  Copyright © 2015 codesplice pty ltd. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for StarWars-OSX.
FOUNDATION_EXPORT double StarWars_OSXVersionNumber;

//! Project version string for StarWars-OSX.
FOUNDATION_EXPORT const unsigned char StarWars_OSXVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StarWars_OSX/PublicHeader.h>


