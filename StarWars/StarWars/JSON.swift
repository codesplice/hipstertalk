//
//  JSON.swift
//  StarWars
//
//  Created by Sam Ritchie on 26/08/2015.
//  Copyright © 2015 codesplice pty ltd. All rights reserved.
//

import Foundation
import Argo 
import Curry

func toDouble(string: String) -> Decoded<Double> {
    return Decoded<Double>.fromOptional(Double(string))
}

func toOptionalInt(string: String) -> Decoded<Int?> {
    return Decoded<Int?>.Success(Int(string))
}

func toOptionalDouble(string: String) -> Decoded<Double?> {
    return Decoded<Double?>.Success(Double(string))
}

func toIntOrZero(string: String) -> Decoded<Int> {
    return Decoded<Int>.Success(Int(string) ?? 0)
}
extension Person: Decodable {
    public static func decode(j: JSON) -> Decoded<Person> {
        let f = curry(Person.init)
            <^> j <| "name"
            <*> j <| "birth_year"
            <*> j <| "eye_color"
            <*> j <| "gender"
            <*> j <| "hair_color"
            <*> (j <| "height" >>- toOptionalInt)
            <*> (j <| "mass" >>- toOptionalDouble)
        return f <*> j <| "skin_color"
            <*> j <| "homeworld"
            <*> j <|| "films"
            <*> j <|| "species"
            <*> j <|| "starships"
            <*> j <|| "vehicles"
            <*> j <| "url"
    }
}

extension Film: Decodable {
    public static func decode(j: JSON) -> Decoded<Film> {
        let f = curry(Film.init)
            <^> j <| "title"
            <*> j <| "episode_id"
            <*> j <| "opening_crawl"
            <*> j <| "director"
            <*> j <| "producer"
            <*> j <| "release_date"
            <*> j <|| "species"
        return f <*> j <|| "starships"
            <*> j <|| "vehicles"
            <*> j <|| "characters"
            <*> j <|| "planets"
            <*> j <| "url"
    }
}

extension Starship: Decodable {
    public static func decode(j: JSON) -> Decoded<Starship> {
        let f = curry(Starship.init)
            <^> j <| "name"
            <*> j <| "model"
            <*> j <| "starship_class"
            <*> j <| "manufacturer"
            <*> (j <| "cost_in_credits" >>- toOptionalInt)
            <*> (j <| "length" >>- toDouble)
            <*> (j <| "crew" >>- toIntOrZero)
            <*> (j <| "passengers" >>- toIntOrZero)
        return f <*> (j <| "max_atmosphering_speed" >>- toOptionalDouble)
            <*> j <| "hyperdrive_rating"
            <*> j <| "MGLT"
            <*> (j <| "cargo_capacity" >>- toIntOrZero)
            <*> j <|| "films"
            <*> j <|| "pilots"
            <*> j <| "url"
    }
}

extension Vehicle: Decodable {
    public static func decode(j: JSON) -> Decoded<Vehicle> {
        let f = curry(Vehicle.init)
            <^> j <| "name"
            <*> j <| "model"
            <*> j <| "vehicle_class"
            <*> j <| "manufacturer"
            <*> (j <| "length" >>- toDouble)
            <*> (j <| "cost_in_credits" >>- toOptionalInt)
            <*> (j <| "crew" >>- toIntOrZero)
            <*> (j <| "passengers" >>- toIntOrZero)
        return f <*> (j <| "max_atmosphering_speed" >>- toDouble)
            <*> (j <| "cargo_capacity" >>- toIntOrZero)
            <*> j <|| "films"
            <*> j <|| "pilots"
            <*> j <| "url"
    }
}

extension Species: Decodable {
    public static func decode(j: JSON) -> Decoded<Species> {
        let f = curry(Species.init)
            <^> j <| "name"
            <*> j <| "classification"
            <*> j <| "designation"
            <*> (j <| "average_height" >>- toOptionalInt)
            <*> (j <| "average_lifespan" >>- toOptionalInt)
            <*> j <| "eye_colors"
            <*> j <| "hair_colors"
        return f <*> j <| "skin_colors"
            <*> j <| "language"
            <*> j <|? "homeworld"
            <*> j <|| "people"
            <*> j <|| "films"
            <*> j <| "url"
    }
}

extension Planet: Decodable {
    public static func decode(j: JSON) -> Decoded<Planet> {
        let f = curry(Planet.init)
            <^> j <| "name"
            <*> (j <| "diameter" >>- toOptionalInt)
            <*> (j <| "rotation_period" >>- toOptionalInt)
            <*> (j <| "orbital_period" >>- toOptionalInt)
            <*> (j <| "gravity" >>- toOptionalDouble)
            <*> (j <| "population" >>- toOptionalInt)
            <*> j <| "climate"
        return f <*> j <| "terrain"
            <*> (j <| "surface_water" >>- toOptionalInt)
            <*> j <|| "residents"
            <*> j <|| "films"
            <*> j <| "url"
    }
}

func loadFromFile<T: Decodable where T == T.DecodedType>(file: String) -> [T] {
    let data = NSData(contentsOfFile: NSBundle(identifier: "au.com.codesplice.StarWars")!.pathForResource(file, ofType: "json")!)
    return [T].decode(JSON.parse(try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)))).value!
}

public let people: [Person] = loadFromFile("people")
public let films: [Film] = loadFromFile("films")
public let starships: [Starship] = loadFromFile("starships")
public let vehicles: [Vehicle] = loadFromFile("vehicles")
public let species: [Species] = loadFromFile("species")
public let planets: [Planet] = loadFromFile("planets")

extension Person {
    public init(url: String) {
        self = people.filter({ $0.url == url }).first!
    }
}

extension Film {
    public init(url: String) {
        self = films.filter({ $0.url == url }).first!
    }
}

extension Starship {
    public init(url: String) {
        self = starships.filter({ $0.url == url }).first!
    }
}

extension Vehicle {
    public init(url: String) {
        self = vehicles.filter({ $0.url == url }).first!
    }
}

extension Species {
    public init(url: String) {
        self = species.filter({ $0.url == url }).first!
    }
}

extension Planet {
    public init(url: String) {
        self = planets.filter({ $0.url == url }).first!
    }
}
