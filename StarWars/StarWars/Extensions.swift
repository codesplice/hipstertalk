//
//  Extensions.swift
//  StarWars
//
//  Created by Sam Ritchie on 26/08/2015.
//  Copyright © 2015 codesplice pty ltd. All rights reserved.
//

import Foundation

public extension SequenceType where Generator.Element: Comparable {
    public func distinct() -> [Generator.Element] {
        var arr : [Generator.Element] = []
        for e in self {
            if !arr.contains(e) { arr.append(e) }
        }
        return arr
    }
}

public extension SequenceType {
    public func groupBy<K: Comparable where K: Hashable>(keySelector: (Generator.Element) -> K) -> [K: [Generator.Element]] {
        return self.reduce([:]) { (var d: [K: [Generator.Element]], e: Generator.Element) ->  [K: [Generator.Element]] in
            let key = keySelector(e)
            if let arr = d[key] {
                d[key] = arr + [e]
            } else {
                d[key] = [e]
            }
            return d
        }
    }
}

public extension SequenceType where Generator.Element == Int {
    public func average() -> Double {
        let (count, sum) = self.reduce((0, 0)) { (a: (Int, Int), e: Generator.Element) -> (Int, Int) in
            return (a.0 + 1, a.1 + e)
        }
        return Double(sum) / Double(count)
    }
}

public extension String {
    public func split() -> [String] {
        return self.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: ", "))
            .filter { $0 != "" }
    }
}
