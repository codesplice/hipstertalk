//
//  StarWars.swift
//  StarWars
//
//  Created by Sam Ritchie on 26/08/2015.
//  Copyright © 2015 codesplice pty ltd. All rights reserved.
//

import Foundation

public struct Person: CustomDebugStringConvertible {
    public let name: String                // The name of this person.
    public let birthYear: String           // The birth year of the person, using the in-universe standard of BBY or ABY
    public let eyeColor: String            // The eye color of this person. Will be "unknown" if not known or "n/a" if the person does not have an eye.
    public let gender: String              // The gender of this person. Either "Male", "Female" or "unknown", "n/a" if the person does not have a gender.
    public let hairColor: String           // The hair color of this person. Will be "unknown" if not known or "n/a" if the person does not have hair.
    public let height: Int?                // The height of the person in centimeters.
    public let mass: Double?               // The mass of the person in kilograms.
    public let skinColor: String           // The skin color of this person.
    public let homeworld: String           // The URL of a planet resource, a planet that this person was born on or inhabits.
    public let films: [String]             // An array of film resource URLs that this person has been in.
    public let species: [String]           // An array of species resource URLs that this person belonds to.
    public let starships: [String]         // An array of starship resource URLs that this person has piloted.
    public let vehicles: [String]          // An array of vehicle resource URLs that this person has piloted.
    public let url: String                 // the hypermedia URL of this resource.
    
    public var debugDescription: String { return name }
}

public struct Film: CustomDebugStringConvertible {
    public let title: String               // The title of this film
    public let episodeId: Int              // The episode number of this film.
    public let openingCrawl: String        // The opening paragraphs at the beginning of this film.
    public let director: String            // The name of the director of this film.
    public let producer: String            // The name(s) of the producer(s) of this film. Comma seperated.
    public let releaseDate: String         // The ISO 8601 date format of film release at original creator country.
    public let species: [String]           // An array of species resource URLs that are in this film.
    public let starships: [String]         // An array of starship resource URLs that are in this film.
    public let vehicles: [String]          // An array of vehicle resource URLs that are in this film.
    public let characters: [String]        // An array of people resource URLs that are in this film.
    public let planets: [String]           // An array of planet resource URLs that are in this film.
    public let url: String                 // the hypermedia URL of this resource.
    
    public var debugDescription: String { return title }
}

public struct Starship: CustomDebugStringConvertible {
    public let name: String                // The name of this starship. The common name, such as "Death Star".
    public let model: String               // The model or official name of this starship. Such as "T-65 X-wing" or "DS-1 Orbital Battle Station".
    public let starshipClass: String       // The class of this starship, such as "Starfighter" or "Deep Space Mobile Battlestation"
    public let manufacturer: String        // The manufacturer of this starship. Comma seperated if more than one.
    public let costInCredits: Int?         // The cost of this starship new, in galactic credits.
    public let length: Double              // The length of this starship in meters.
    public let crew: Int                   // The number of personnel needed to run or pilot this starship.
    public let passengers: Int             // The number of non-essential people this starship can transport.
    public let maxAtmospheringSpeed: Double? // The maximum speed of this starship in atmosphere. "N/A" if this starship is incapable of atmosphering flight.
    public let hyperdriveRating: String    // The class of this starships hyperdrive.
    public let MGLT: String                // The Maximum number of Megalights this starship can travel in a standard hour.
    public let cargoCapacity: Int          // The maximum number of kilograms that this starship can transport.
    public let films: [String]             // An array of Film URL Resources that this starship has appeared in.
    public let pilots: [String]            // An array of People URL Resources that this starship has been piloted by.
    public let url: String                 // the hypermedia URL of this resource.
    
    public var debugDescription: String { return name }
}

public struct Vehicle: CustomDebugStringConvertible {
    public let name: String                // The name of this vehicle. The common name, such as "Sand Crawler" or "Speeder bike".
    public let model: String               // The model or official name of this vehicle. Such as "All-Terrain Attack Transport".
    public let vehicleClass: String        // The class of this vehicle, such as "Wheeled" or "Repulsorcraft".
    public let manufacturer: String        // The manufacturer of this vehicle. Comma-seperated if more than one.
    public let length: Double              // The length of this vehicle in meters.
    public let costInCredits: Int?         // The cost of this vehicle new, in Galactic Credits.
    public let crew: Int                   // The number of personnel needed to run or pilot this vehicle.
    public let passengers: Int             // The number of non-essential people this vehicle can transport.
    public let maxAtmospheringSpeed: Double // The maximum speed of this vehicle in atmosphere.
    public let cargoCapacity: Int          // The maximum number of kilograms that this vehicle can transport.
    public let films: [String]             // An array of Film URL Resources that this vehicle has appeared in.
    public let pilots: [String]            // An array of People URL Resources that this vehicle has been piloted by.
    public let url: String                 // the hypermedia URL of this resource.
    
    public var debugDescription: String { return name }
}

public struct Species: CustomDebugStringConvertible {
    public let name: String                // The name of this species.
    public let classification: String      // The classification of this species, such as "mammal" or "reptile".
    public let designation: String         // The designation of this species, such as "sentient".
    public let averageHeight: Int?         // The average height of this species in centimeters.
    public let averageLifespan: Int?       // The average lifespan of this species in years.
    public let eyeColors: String           // A comma-seperated string of common eye colors for this species, "none" if this species does not typically have eyes.
    public let hairColors: String          // A comma-seperated string of common hair colors for this species, "none" if this species does not typically have hair.
    public let skinColors: String          // A comma-seperated string of common skin colors for this species, "none" if this species does not typically have skin.
    public let language: String            // The language commonly spoken by this species.
    public let homeworld: String?          // The URL of a planet resource, a planet that this species originates from.
    public let people: [String]            // An array of People URL Resources that are a part of this species.
    public let films: [String]             // An array of Film URL Resources that this species has appeared in.
    public let url: String                 // the hypermedia URL of this resource.
    
    public var debugDescription: String { return name }
}

public struct Planet: CustomDebugStringConvertible {
    public let name: String                // The name of this planet.
    public let diameter: Int?              // The diameter of this planet in kilometers.
    public let rotationPeriod: Int?        // The number of standard hours it takes for this planet to complete a single rotation on its axis.
    public let orbitalPeriod: Int?         // The number of standard days it takes for this planet to complete a single orbit of its local star.
    public let gravity: Double?            // A number denoting the gravity of this planet, where "1" is normal or 1 standard G. "2" is twice or 2 standard Gs. "0.5" is half or 0.5 standard Gs.
    public let population: Int?            // The average population of sentient beings inhabiting this planet.
    public let climate: String             // The climate of this planet. Comma-seperated if diverse.
    public let terrain: String             // The terrain of this planet. Comma-seperated if diverse.
    public let surfaceWater: Int?          // The percentage of the planet surface that is naturally occuring water or bodies of water.
    public let residents: [String]         // An array of People URL Resources that live on this planet.
    public let films: [String]             // An array of Film URL Resources that this planet has appeared in.
    public let url: String                 // the hypermedia URL of this resource.
    
    public var debugDescription: String { return name }
}

